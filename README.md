# Shopping-Web-Angular-Spring-Boot

## Getting started

-File back-end 
    Edit file application account oracle then run
-File font-end run npm-i (install node_modules) then run ng-serve (run app)

## Project status
-Login -register: success
-Check role: 
    admin -> admin-layout
    user -> user-layout
-Admin-layout:
    CRUD Product Category Profile
-User-layout:
    Add cart
    Checkout
    Payment
-Incomplete:
    Page Order-history (done api missing ui)
    Send mail (google send mail error wait fix)
    Paypal 
    Validation

