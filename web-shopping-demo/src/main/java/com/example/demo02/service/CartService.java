package com.example.demo02.service;

import com.example.demo02.model.Cart;
import com.example.demo02.model.Order;
import com.example.demo02.model.Product;
import com.example.demo02.payload.request.CartRequest;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.responsitory.CartRespository;
import com.example.demo02.responsitory.OrderRepository;
import com.example.demo02.responsitory.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {

    @Autowired
    private CartRespository cartRespository;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderRepository orderrepository;

    public ResponseEntity<?> saveCart(CartRequest cartRequest){
        try {
            Product product = productService.findProductById(cartRequest.getProductId());
            //new cart
            Cart cart = new Cart();
            cart.setQuantity(cartRequest.getQuantity());
            cart.setUserId(cartRequest.getUserId());
            cart.setProduct(product);
            cart.setCheckout(false);
            cartRespository.save(cart);
            return ResponseEntity
                    .ok(new MessageResponse("Add cart success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> updateCart(Cart cart){
        try {
            //find cart and update quantity
            Cart cartObj = cartRespository.findById(cart.getId())
                    .orElseThrow(() -> new Exception("Id not found"));
            cartObj.setQuantity(cart.getQuantity());
            cartRespository.save(cartObj);
            return ResponseEntity
                    .ok(new MessageResponse("Update cart success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> deleteCart(Long id){
        try{
            List<Order> orders = orderrepository.findByCartId(id);
            for (Order order: orders){
                order.setCart(null);
                orderrepository.save(order);
            }
            Cart cart = cartRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id not found"));
            cart.setProduct(null);
            cartRespository.save(cart);
            cartRespository.deleteById(id);
            return ResponseEntity
                    .ok(new MessageResponse("Delete cart success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }

    }

    public ResponseEntity<?> getAllCart(){
        try {
            List<Cart> cart = cartRespository.findAll();
            List<Cart> cartList = new ArrayList<>();
            for (Cart value: cart){
                if (!value.isCheckout()){
                    cartList.add(value);
                    if (value.getProduct()==null){
                        Product product = new Product("da xoa", 0, 0 , null);
                        value.setProduct(product);
                    }
                }
            }
            return ResponseEntity
                    .ok(cartList);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }
}
