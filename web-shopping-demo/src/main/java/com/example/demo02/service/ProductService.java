package com.example.demo02.service;

import com.example.demo02.model.Cart;
import com.example.demo02.model.Category;
import com.example.demo02.model.Product;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.responsitory.CartRespository;
import com.example.demo02.responsitory.CategoryRepository;
import com.example.demo02.responsitory.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRespository;

    @Autowired
    CategoryRepository categoryRespository;

    @Autowired
    CartRespository cartRespository;

    public ResponseEntity<?> saveProduct(String name, int price, int totals, Long category_id, MultipartFile file){
        try {
            Category category = categoryRespository.findById(category_id)
                    .orElseThrow(() -> new Exception("Id update not found"));
            Product newP = new Product(name, price, totals, file.getBytes());
            newP.setCategory(category);
            productRespository.save(newP);
            return ResponseEntity
                    .ok(new MessageResponse("Add product success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> getAllProducts(){
        try {
            List<Product> list = productRespository.findAll();
            for (Product value: list){
                if(value.getCategory()==null){
                    Category category = new Category("da xoa", "error");
                    value.setCategory(category);
                }
            }
            return ResponseEntity
                    .ok(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> upadteProduct(Long id, String name, int price, int totals, Long category_id, MultipartFile file){
        try {
            Category category = categoryRespository.findById(category_id)
                    .orElseThrow(() -> new Exception("Id update not found"));
            Product productObj = productRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id update not found"));
            productObj.setName(name);
            productObj.setPrice(price);
            productObj.setTotals(totals);
            productObj.setCategory(category);
            productObj.setPicByte(file.getBytes());
            productRespository.save(productObj);
            return ResponseEntity
                    .ok(new MessageResponse("Update product success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> upadteProductNoImg(Long id, String name, int price, int totals, Long category_id){
        try {
            Category category = categoryRespository.findById(category_id)
                    .orElseThrow(() -> new Exception("Id update not found"));
            Product productObj = productRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id update not found"));
            productObj.setName(name);
            productObj.setPrice(price);
            productObj.setTotals(totals);
            productObj.setCategory(category);
            productRespository.save(productObj);
            return ResponseEntity
                    .ok(new MessageResponse("Update product success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> deleteProduct(Long id){
        try {
            List<Cart> carts = cartRespository.findByProductId(id);
            for (Cart value: carts){
                value.setProduct(null);
                cartRespository.save(value);
            }
            Product product = productRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id not found"));
            product.setCategory(null);
            productRespository.save(product);
            productRespository.deleteById(id);
            return ResponseEntity
                    .ok(new MessageResponse("Delete product success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public Product findProductById(Long id) throws Exception{
        return productRespository.findById(id).orElseThrow(() -> new Exception("Id not found"));
    }

    public ResponseEntity<?> findProductByName(String name){
        try {
            List<Product> list = productRespository.findByNameContaining(name);
            for (Product value: list){
                if(value.getCategory()==null){
                    Category category = new Category("da xoa", "error");
                    value.setCategory(category);
                }
            }
            return ResponseEntity
                    .ok(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }
//
//    public static byte[] compressBytes(byte[] data) {
//        Deflater deflater = new Deflater();
//        deflater.setInput(data);
//        deflater.finish();
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
//        byte[] buffer = new byte[1024];
//        while (!deflater.finished()) {
//            int count = deflater.deflate(buffer);
//            outputStream.write(buffer, 0, count);
//        }
//        try {
//            outputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return outputStream.toByteArray();
//    }
//
//    public static byte[] decompressBytes(byte[] data) {
//        Inflater inflater = new Inflater();
//        inflater.setInput(data);
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
//        byte[] buffer = new byte[1024];
//        try {
//            while (!inflater.finished()) {
//                int count = inflater.inflate(buffer);
//                outputStream.write(buffer, 0, count);
//            }
//            outputStream.close();
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//        return outputStream.toByteArray();
//    }
}
