package com.example.demo02.service;

import com.example.demo02.model.Category;
import com.example.demo02.model.Product;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.responsitory.CategoryRepository;
import com.example.demo02.responsitory.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOError;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRespository;

    @Autowired
    private ProductRepository productRespository;

    public ResponseEntity<?> saveCategory(Category category){
        try{
            categoryRespository.save(category);
            return ResponseEntity
                    .ok(new MessageResponse("Add category success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }

    }

    public ResponseEntity<?> getAllCategories(){
        try{
            List<Category> list = categoryRespository.findAll();
            return ResponseEntity
                    .ok(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public Category findCategoryById(Long id) throws Exception {
        return categoryRespository.findById(id)
                .orElseThrow(() -> new Exception("Id not found"));
    }

    public ResponseEntity<?> updateCategory(Category category){
        try {
            Category newcategory = categoryRespository.findById(category.getId())
                    .orElseThrow(() -> new Exception("Id not found"));
            newcategory.setName(category.getName());
            newcategory.setTitle(category.getTitle());
            categoryRespository.save(newcategory);
            return ResponseEntity
                    .ok(new MessageResponse("Update category success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> deleteCategory(Long id){
        try {
            List<Product> products = productRespository.findByCategoryId(id);
            for (Product value: products){
                value.setCategory(null);
                productRespository.save(value);
            }
            categoryRespository.deleteById(id);
            return ResponseEntity
                    .ok(new MessageResponse("Delete category success"));
        }catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }
}
