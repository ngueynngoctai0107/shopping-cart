package com.example.demo02.service;

import com.example.demo02.model.User;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.responsitory.UserRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Service
public class UserService {

    @Autowired
    UserRespository userRespository;

    @Autowired
    PasswordEncoder encoder;

    public ResponseEntity<?> findUserByUsername(String username){
        try {
            User user = userRespository.findByUsername(username).orElseThrow(() -> new Exception("Username not found"));
            if (user.getAvatar()!=null) {
                user.setAvatar(user.getAvatar());
            }
            return ResponseEntity
                    .ok(user);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> upadteUser(Long id, String fullName){
        try {
            User userObj = userRespository.findById(id)
                    .orElseThrow();
            userObj.setFullName(fullName);
            userRespository.save(userObj);
            return ResponseEntity
                    .ok(new MessageResponse("Update user success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> upadteAvatar(Long id, MultipartFile file){
        try {
            User userObj = userRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id not found"));
            userObj.setAvatar(file.getBytes());
            userRespository.save(userObj);
            return ResponseEntity
                    .ok(new MessageResponse("Update avatar success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> updatePassword(Long id, String oldPass, String newPass){
        try {
            User userObj = userRespository.findById(id)
                    .orElseThrow(() -> new Exception("Id not found"));
            if (encoder.matches(oldPass, userObj.getPassword())){
                userObj.setPassword(encoder.encode(newPass));
                userRespository.save(userObj);
                return ResponseEntity
                        .ok(new MessageResponse("Update password success"));
            }
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Password is incorrect"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

}
