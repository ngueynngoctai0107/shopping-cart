package com.example.demo02.service;

import com.example.demo02.model.Cart;
import com.example.demo02.model.Order;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.responsitory.CartRespository;
import com.example.demo02.responsitory.OrderRepository;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    CartRespository cartRespository;

    @Autowired
    OrderRepository orderrepository;

    public ResponseEntity<?> checkout(Long id, String name, int phone, String address, int totals, boolean isCheckout){
        List<Cart> cart = cartRespository.findByUserId(id);
        List<Cart> cartList = new ArrayList<>();
        try {
            for (Cart value: cart){
                if (!value.isCheckout()){
                    cartList.add(value);
                    //update cart checkout
                    value.setCheckout(true);
                    cartRespository.save(value);
                }
            }

            Order order = new Order();
            order.setName(name);
            order.setPhone(phone);
            order.setUserId(id);
            order.setCart(cartList);
            order.setAddress(address);
            order.setTotals(totals);
            order.setCheckout(isCheckout);
            orderrepository.save(order);
            return ResponseEntity
                    .ok(new MessageResponse("Checkout success"));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }

    public ResponseEntity<?> getAllOrders(){
        try{
            List<Order> list = orderrepository.findAll();
            return ResponseEntity
                    .ok(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: " + e));
        }
    }
}
