package com.example.demo02.payload.request;

import lombok.Data;

@Data
public class CartRequest {

    private int quantity;

    private Long productId;

    private Long userId;
}
