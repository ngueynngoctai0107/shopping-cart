package com.example.demo02.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity @Table(name = "T_TABLE_ORDERS")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Long userId;

    String name;

    String address;

    int phone;

    int totals;

    boolean isCheckout;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Cart> cart;
}
