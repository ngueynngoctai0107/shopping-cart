package com.example.demo02.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "T_TABLE_PRODUCTS")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    private int price;

    private int totals;

    @ManyToOne(cascade = CascadeType.ALL)
    private Category category;

    @Column(name = "picByte", length = 1000)
    private byte[] picByte;

    public Product(String name, int price, int totals, byte[] picByte) {
        this.name = name;
        this.price = price;
        this.totals = totals;
        this.picByte = picByte;
    }
}
