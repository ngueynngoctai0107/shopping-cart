package com.example.demo02.controller;

import com.example.demo02.model.Product;
import com.example.demo02.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(path = "" )
    public ResponseEntity<?> getAll() {
        return productService.getAllProducts();
    }

    @PostMapping("/insert")
    public ResponseEntity<?> saveProduct(@RequestPart("name") String name,
                               @RequestPart("price") int price,
                               @RequestPart("totals") int totals,
                               @RequestPart("category_id") Long category_id,
                               @RequestParam("image") MultipartFile file){
        return productService.saveProduct(name,price,totals,category_id,file);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateProduct(@RequestPart("id") Long id,
                                                 @RequestPart("name") String name,
                                                 @RequestPart("price") int price,
                                                 @RequestPart("totals") int totals,
                                                 @RequestPart("category_id") Long category_id,
                                                 @RequestPart("image") MultipartFile file){
        return productService.upadteProduct(id,name,price,totals,category_id,file);
    }

    @PutMapping("/update-no-img/{id}")
    public ResponseEntity<?> upadteProductNoImg(@RequestPart("id") Long id,
                                                 @RequestPart("name") String name,
                                                 @RequestPart("price") int price,
                                                 @RequestPart("totals") int totals,
                                                 @RequestPart("category_id") Long category_id){
        return productService.upadteProductNoImg(id,name,price,totals,category_id);
    }

    @GetMapping("/find/{id}")
    public Product findProductById(@PathVariable Long id) throws Exception {
        return productService.findProductById(id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable Long id){
        return productService.deleteProduct(id);
    }

    @PostMapping("/findName")
    public ResponseEntity<?> findProductByName(@RequestParam String name){
        return  productService.findProductByName(name);
    }
}
