package com.example.demo02.controller;

import com.example.demo02.model.Order;
import com.example.demo02.responsitory.OrderRepository;
import com.example.demo02.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderrepository;

    @PostMapping("/checkout")
    public ResponseEntity<?> checkout(@RequestPart("id") Long id, @RequestPart("name") String name,
                                      @RequestPart("phone") int phone, @RequestPart("address") String address,
                                      @RequestPart("totals") int totals, @RequestPart("isCheckout") boolean isCheckout){
        return orderService.checkout(id, name, phone, address, totals, isCheckout);
    }

    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return orderService.getAllOrders();
    }
}
