package com.example.demo02.controller;

import com.example.demo02.model.Cart;
import com.example.demo02.payload.request.CartRequest;
import com.example.demo02.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping("/add")
    public ResponseEntity<?> saveCart(@RequestBody CartRequest cartRequest){
        return cartService.saveCart(cartRequest);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateCart(@RequestBody Cart cart){
        return cartService.updateCart(cart);
    }

    @GetMapping("")
    public ResponseEntity<?> getAllCart(){
        return cartService.getAllCart();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategoryById(@PathVariable Long id) {
        return cartService.deleteCart(id);
    }

}
