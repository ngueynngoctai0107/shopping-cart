package com.example.demo02.controller;

import com.example.demo02.model.User;
import com.example.demo02.payload.response.MessageResponse;
import com.example.demo02.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/find/{username}")
    public ResponseEntity<?> findUserByUsername(@PathVariable String username){
        return userService.findUserByUsername(username);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@RequestPart("id") Long id,
                                              @RequestPart("fullName") String fullName){
        return userService.upadteUser(id,fullName);
    }

    @PutMapping("/update-avatar/{id}")
    public ResponseEntity<?> updateAvatarUser(@RequestPart("id") Long id, @RequestParam("avatar") MultipartFile file){
        return userService.upadteAvatar(id,file);
    }

    @PutMapping("/update-password/{id}")
    public ResponseEntity<?> updatePasswordUser(@RequestPart("id") Long id,
                                           @RequestPart("passwordOld") String oldPass,
                                           @RequestPart("passwordNew") String newPass){
        return userService.updatePassword(id,oldPass,newPass);
    }
}
