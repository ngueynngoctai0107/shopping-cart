package com.example.demo02.controller;

import com.example.demo02.model.Category;
import com.example.demo02.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/add")
    public ResponseEntity<?> saveCategory(@RequestBody Category category){
        return categoryService.saveCategory(category);
    }

    @GetMapping("")
    public ResponseEntity<?> getAllCategory(){
        return categoryService.getAllCategories();
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateCategory(@RequestBody Category category){
        return categoryService.updateCategory(category);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategoryById(@PathVariable Long id) {
        return categoryService.deleteCategory(id);
    }

    @GetMapping("/find/{id}")
    public Category findCategoryById(@PathVariable Long id) throws Exception {
        return categoryService.findCategoryById(id);
    }
}
