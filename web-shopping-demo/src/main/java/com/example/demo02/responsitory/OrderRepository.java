package com.example.demo02.responsitory;

import com.example.demo02.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    public List<Order> findByCartId(Long id);
}
