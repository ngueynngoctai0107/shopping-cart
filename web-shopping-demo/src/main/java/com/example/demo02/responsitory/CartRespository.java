package com.example.demo02.responsitory;

import com.example.demo02.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CartRespository extends JpaRepository<Cart, Long> {
    public List<Cart> findByUserId(Long id);

    public List<Cart> findByProductId(Long id);
}
