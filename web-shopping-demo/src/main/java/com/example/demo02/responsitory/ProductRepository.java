package com.example.demo02.responsitory;

import com.example.demo02.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    public List<Product> findByNameContaining(String name);

    public List<Product> findByCategoryId(Long id);
}
